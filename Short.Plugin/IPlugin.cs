﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Short.Plugin
{
    public interface IPlugin
    {
        /// <summary>
        /// Void Init is run at startup Short
        /// </summary>
        /// <returns>Returns the Plugin informations</returns>
        PluginMetaInfo Init();
        /// <summary>
        /// Function which is run when the user carry out a search
        /// </summary>
        /// <param name="query">It's the words searched by the user</param>
        /// <returns>Return a list of QueryResult</returns>
        List<QueryResult> Query(String query);
    }
}
