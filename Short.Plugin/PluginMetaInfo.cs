﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Short.Plugin
{
    public class PluginMetaInfo
    {
        public PluginMetaInfo(string Api)
        {
            if (string.IsNullOrWhiteSpace(Api) || Api.Length != 32)
            {
                throw new Exception("The syntaxe api is not correct !");
            }
            else
            {
                API = Api;
            }
        }

        /// <summary>
        /// It will a official code from the Store to avoid malwares
        /// </summary>
        public string API { get; private set; }
        /// <summary>
        /// This property define the plugin name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// This property define the author of plugin
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// This property define the plugin version
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// This property define the language of his plugin
        /// </summary>
        public string Language { get; set; }
        /// <summary>
        /// This property define the description for your plugin
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// List of words triggering the plugin if null always triggered
        /// </summary>
        public List<string> ActionKeywords { get; set; }

        /// <summary>
        /// This property define the icon path for this plugin
        /// </summary>
        public string IconPath { get; set; }
        /// <summary>
        /// This property define the website path of plugin
        /// </summary>
        public Uri Website { get; set; }
    }
}
