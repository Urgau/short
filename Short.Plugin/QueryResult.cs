﻿using System;
using System.Diagnostics;
using System.IO;

namespace Short.Plugin
{
    public class QueryResult
    {
        public String IcoPath { get; private set; }
        public String Title { get; private set; }
        public String Path { get; private set; }
        public Action Execute { get; private set; }

        /// <param name="icoPath">Path of image query</param>
        /// <param name="title">Title of query</param>
        /// <param name="path">Link of query</param>
        /// <param name="execute">Action execute when the user click of this query</param>
        public QueryResult(String icoPath, String title, String path, Action execute)
        {
            IcoPath = icoPath; Title = title; Path = path; Execute = execute;
        }

        /// <param name="icoPath">Path of image query</param>
        /// <param name="title">Title of query</param>
        /// <param name="path">Link of the query to will be executed by Process.Start() if this result is select</param>
        public QueryResult(String icoPath, String title, String path)
        {
            IcoPath = icoPath; Title = title; Path = path; Execute = new Action(() => { try { Process.Start(path); } catch (Exception ex) { Console.WriteLine(Title + " (" + path + ") erreur lors du lancement : " + ex.ToString()); } });
        }
    }
}
