﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Timers;
using System.Windows.Media;

namespace Short
{
    public static class RegistryExtension
    {
        public static bool ExistSubKey(this RegistryKey reg, string name, out RegistryKey RegistrySub)
        {
            RegistryKey regSub = reg.OpenSubKey(name, true);
            if (regSub != null) { RegistrySub = regSub; return true; }
            else { RegistrySub = null; return false; }
        }

        public static bool ExistKey(this RegistryKey reg, string key, out object Key)
        {
            object obj = reg.GetValue(key, true);
            if (obj != null) { Key = obj; return true; }
            else { Key = null; return false; }
        }

        public static RegistryKey OpenOrCreateSubKey(this RegistryKey reg, string name)
        {
            RegistryKey regSub = reg.OpenSubKey(name, true);
            if (regSub != null) { return regSub; }
            else { return reg.CreateSubKey(name, RegistryKeyPermissionCheck.ReadWriteSubTree); }
        }
    }

    class Utilities
    {
        public static RegistryKey RegistryApp { get { return Registry.CurrentUser.OpenOrCreateSubKey(@"Software\Short"); } }
        public static RegistryKey RegistryDisable { get { return RegistryApp.OpenOrCreateSubKey("Disable"); } }

        public class MinuteurAction
        {
            private Action Action;
            private System.Windows.Threading.Dispatcher disGui = System.Windows.Threading.Dispatcher.CurrentDispatcher;
            private Timer timer;
            private int Delay = 0;
            private int DelayBase = 0;

            public MinuteurAction(int delayBase)
            {
                timer = new Timer();
                timer.Elapsed += timer_Elapsed;
                timer.Interval = 5;
                DelayBase = delayBase;
            }

            public void SetAction(Action action)
            {
                Action = action;
            }

            void timer_Elapsed(object sender, ElapsedEventArgs e)
            {
                if (Delay == 0)
                {
                    timer.Stop(); //Le laisser ici sinon bug !!!
                    disGui.Invoke(Action);
                }
                else
                {
                    Delay--;
                }
            }

            public void StartDelay()
            {
                Delay = DelayBase;
                timer.Start();
            }
        }

        public sealed class Startup
        {
            const string RegistryStartupPath = @"Software\Microsoft\Windows\CurrentVersion\Run";
            static string ExePath = System.Reflection.Assembly.GetEntryAssembly().Location;
            static string ExePathFileName = Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location);

            public static bool AddStartup()
            {
                try
                {
                    if (!ExistStartup())
                    {
                        Registry.CurrentUser.CreateSubKey(RegistryStartupPath).SetValue(ExePathFileName, ExePath);
                        return true;
                    }
                    return false;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    return false;
                }
            }

            public static bool DeleteStartup()
            {
                try
                {
                    RegistryKey rk = Registry.CurrentUser.OpenSubKey(RegistryStartupPath, true);
                    if (rk.GetValue(ExePathFileName) == null) { return false; }
                    else
                    {
                        rk.DeleteValue(ExePathFileName);
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    return false;
                }
            }

            public static bool ExistStartup()
            {
                return Registry.CurrentUser.OpenSubKey(RegistryStartupPath).GetValue(ExePathFileName) == null ? false : true;
            }
        }

        public sealed class CacheImage
        {
            private static Dictionary<String, WeakReference> lCacheImg = new Dictionary<string, WeakReference>();

            public static bool PathExist(string path)
            {
                if (lCacheImg.ContainsKey(path))
                {
                    if (lCacheImg[path].IsAlive == false)
                    {
                        lCacheImg.Remove(path);
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }

            public static ImageSource GetImageSource(string path)
            {
                return PathExist(path) ? (ImageSource)lCacheImg[path].Target : null;
            }

            public static void AddImageSourceInCache(string path, ImageSource img)
            {
                //Limite 100
                lCacheImg.Add(path, new WeakReference(img));
                if (lCacheImg.Count > 100)
                {
                    lCacheImg.Remove(lCacheImg.FirstOrDefault().Key);
                    Console.WriteLine("Cela marche bien ! {0}", lCacheImg.FirstOrDefault().Key);
                }
            }
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
        private static extern short GetKeyState(Int32 i);
        public static bool KeyPress(int i)
        {
            if ((GetKeyState(i) & 0x8000) != 0) { return true; }
            else { return false; }
        }
    }
}
