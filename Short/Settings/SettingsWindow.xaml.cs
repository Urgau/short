﻿using Microsoft.Win32;
using Short.Plugin;
using Short.Settings;
using Short.Themes;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Short
{
    /// <summary>
    /// Logique d'interaction pour SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public SettingsWindow()
        {
            InitializeComponent();

            //Plugins
            LoadPluginsMetaInfo();
            if (listViewPluginMetaInfo.Items.Count > 1) { listViewPluginMetaInfo.SelectedIndex = 0; }

            //Themes
            cbThemes.ItemsSource = ThemeManager.ListThemes;
            cbThemes.SelectedIndex = 0;
            
            //Startup
            cbStartup.IsChecked = Utilities.Startup.ExistStartup();
        }

        List<PluginPair> lPluginPair;
        private void LoadPluginsMetaInfo()
        {
            lPluginPair = PluginsManager.GetPluginsPair();

            foreach (PluginPair PA in lPluginPair)
            {
                listViewPluginMetaInfo.Items.Add(new PluginMetaInfoItem() { PluginName = PA.PluginMetaInfo.Name, IconPath = PA.PluginMetaInfo.IconPath });
            }
        }

        private const string Nothing = "No information";
        private PluginPair PluginAfficher;
        private void ShowPluginMetaInfo(PluginPair PA)
        {
            PluginMetaInfo PMI = PA.PluginMetaInfo;
            ImagePathConverter ipc = new ImagePathConverter();
            PluginIcon.Source = (ImageSource)ipc.PathToImageSource(PMI.IconPath);
            PluginName.Text = string.IsNullOrWhiteSpace(PMI.Name) == false  ? PMI.Name : Nothing;
            PluginAuthor.Text = string.IsNullOrWhiteSpace(PMI.Author) == false ? PMI.Author : Nothing;

            //Version
            if (string.IsNullOrWhiteSpace(PMI.Version)) { PluginVersionContainer.Visibility = Visibility.Collapsed; }
            else { PluginVersionContainer.Visibility = Visibility.Visible; PluginVersion.Text = PMI.Version; }
            //Language
            if (string.IsNullOrWhiteSpace(PMI.Language)) { PluginLanguageContainer.Visibility = Visibility.Collapsed; }
            else { PluginLanguageContainer.Visibility = Visibility.Visible; PluginLanguage.Text = PMI.Language; }
            //Website
            if (PMI.Website == null) { PluginWebsiteContainer.Visibility = Visibility.Collapsed; }
            else { PluginWebsiteContainer.Visibility = Visibility.Visible; PluginWebsite.NavigateUri = PMI.Website; }
            //Language
            if (string.IsNullOrWhiteSpace(PMI.Description)) { PluginDescriptionContainer.Visibility = Visibility.Collapsed; }
            else { PluginDescriptionContainer.Visibility = Visibility.Visible; PluginDescription.Text = PMI.Description; }

            PluginDisable.IsChecked = PA.Disable;
            PluginAfficher = PA;
        }

        private void listViewPluginMetaInfo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = listViewPluginMetaInfo.SelectedIndex;
            if (index >= 0 && index < lPluginPair.Count)
            {
                ShowPluginMetaInfo(lPluginPair[index]);
            }
        }

        private void btnThemes_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ThemeManager.SetTheme(cbThemes.SelectedItem.ToString());
            } catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }

        private void cbStartup_Click(object sender, RoutedEventArgs e)
        {
            cbStartup.IsChecked = Utilities.Startup.ExistStartup() == true ? !Utilities.Startup.DeleteStartup() : Utilities.Startup.AddStartup();
        }

        private void PluginDisable_Click(object sender, RoutedEventArgs e)
        {
            PluginAfficher.SetDisable(PluginDisable.IsChecked.Value);
        }
    }
}
