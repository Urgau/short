﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Short.Settings
{
    public class PluginMetaInfoItem
    {
        public string PluginName { get; set; }
        public string IconPath { get; set; }
    }
}
