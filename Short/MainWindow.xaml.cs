﻿using Short.Plugin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ContextMenu = System.Windows.Forms.ContextMenu;
using NotifyIcon = System.Windows.Forms.NotifyIcon;

namespace Short
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public ContextMenu contextMenu = new ContextMenu();
        private NotifyIcon nIcon = new NotifyIcon()
        {
            Visible = true,
            Icon = Properties.Resources.ShortLogo
        };
        private HotKey hotKey = new HotKey();
        private Utilities.MinuteurAction minuteurAction = new Utilities.MinuteurAction(30);

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location)).Length > 1) //Sup à 1 car ce processuce et compter avec
            {
#if DEBUG
                Process.GetProcessesByName(System.IO.Path.GetFileNameWithoutExtension(System.Reflection.Assembly.GetEntryAssembly().Location))[0].Kill();
#else
                MessageBox.Show("Une autre instance du logiciel est ouverte !\nALT + S pour l'afficher !", "Short", MessageBoxButton.OK);
                Environment.Exit(0);
#endif
            }

            //Def contextmenu
            contextMenu.MenuItems.Add("Show", new EventHandler(contextMenuShow));
            contextMenu.MenuItems.Add("Settings", new EventHandler(contextMenuSettings));
            //contextMenu.MenuItems.Add("About", new EventHandler(contextMenuAbout));
            contextMenu.MenuItems.Add("Exit", new EventHandler(contextMenuExit));
            nIcon.ContextMenu = contextMenu;

            PluginsManager.LoadPlugins();
            PluginsManager.InitPlugins();

            //Position
            var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            this.Left = (desktopWorkingArea.Right / 2) - (this.Width / 2);
            this.Top = (desktopWorkingArea.Bottom * 0.13);

            tbSearch.Focus();
            hotKey.KeysPressed += hotKey_KeysPressed;
            minuteurAction.SetAction(new Action(() => { KeySearch(); }));

            Themes.ThemeManager.LoadLastTheme();
        }

        private void contextMenuShow(object sender, EventArgs e)
        {
            if (!this.IsVisible)
                this.Show();
        }

        private void contextMenuExit(object sender, EventArgs e)
        {
            nIcon.Visible = false;
            Environment.Exit(0);
        }
        
        private void contextMenuSettings(object sender, EventArgs e)
        {
            SettingsWindow settingsWindow = new SettingsWindow();
            settingsWindow.Show();
            if (IsVisible) { this.Hide(); }
        }

        private void KeySearch()
        {
            if (!Utilities.KeyPress(0x08)) //Retour en arrière
            {
                if (tbSearch.Text.Trim().Length >= 2)
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    List<QueryResult> lQuerys = PluginsManager.QueryPlugins(tbSearch.Text.ToLower());
                    if (lQuerys.Count >= 1)
                    {
                        //lbResult.ItemsSource = lQuerys;
                        foreach (QueryResult q in lQuerys) { lbResult.Items.Add(q); }
                        lbResult.SelectedIndex = 0;
                    }
                    sw.Stop();
                    Console.WriteLine("Resultat en " + sw.ElapsedMilliseconds + " ms");
                }
            }
        }

        private void tbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            lbResult.Items.Clear();
            minuteurAction.StartDelay();
        }

        private void tbSearch_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Up || e.Key == Key.Down)
            {
                if (lbResult.Items.Count > 0)
                {
                    int i = e.Key == Key.Up ? lbResult.SelectedIndex - 1 : lbResult.SelectedIndex + 1;
                    if (i < lbResult.Items.Count && i >= 0)
                    {
                        lbResult.SelectedIndex = i;
                        lbResult.ScrollIntoView(i);
                    }
                }
            }
            else if (e.Key == Key.Enter)
            {
                ExecuteQueryResultSelected();
            }
        }

        private void lbResult_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ExecuteQueryResultSelected();
        }

        private void ExecuteQueryResultSelected()
        {
            if (lbResult.SelectedIndex > -1)
            {
                QueryResult q = (QueryResult)lbResult.Items[lbResult.SelectedIndex];
                try
                {
                    q.Execute();
                    tbSearch.Text = "";
                    this.Hide();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(q.Title + "(" + q.Path + ") ! Erreur : " + ex.ToString());
                }
            }
        }

        private void lbResult_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0 && e.AddedItems[0] != null)
            {
                lbResult.ScrollIntoView(e.AddedItems[0]);
            }
        }

        private void hotKey_KeysPressed(object sender, EventArgs e)
        {
            if (this.IsVisible) { this.Hide(); }
            else { this.Show(); tbSearch.Focus(); }
        }
    }
}
