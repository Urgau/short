﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Short.Plugin;
using Microsoft.Win32;

namespace Short
{
    internal sealed class PluginPair
    {
        public IPlugin Plugin { get; private set; }
        public PluginMetaInfo PluginMetaInfo { get; set; }
        public bool Disable { get; private set; }

        public void SetDisable(bool dis) { Disable = dis; Utilities.RegistryDisable.SetValue(PluginMetaInfo.API, dis); }
        public void SetDisableWithOutRegistry(bool dis) { Disable = dis; }
        public PluginPair(IPlugin p) { Plugin = p; }
    }

    sealed class PluginsManager
    {
        private static List<PluginPair> lPlugins = new List<PluginPair>();
        private static readonly String PathPlugins = AppDomain.CurrentDomain.BaseDirectory + @"Plugins\";

        public static void LoadPlugins()
        {
            //Plugin de base
            lPlugins.Add(new PluginPair(new Plugins.Shortcuts()));
            lPlugins.Add(new PluginPair(new Plugins.WebSearch()));
            lPlugins.Add(new PluginPair(new Plugins.Console()));
            lPlugins.Add(new PluginPair(new Plugins.Tinyurl()));
            lPlugins.Add(new PluginPair(new Plugins.CustomCommands()));

            if (Directory.Exists(PathPlugins))
            {
                string[] dllFileNames = Directory.GetFiles(PathPlugins, "*.dll");

                /*****************Solution 1*******************/
                foreach (string pluginPath in dllFileNames)
                {
                    Assembly newAssembly = Assembly.LoadFile(pluginPath);
                    Type[] types = newAssembly.GetTypes();
                    foreach (var type in types)
                    {
                        if (type.IsClass && (type.GetInterface(typeof(IPlugin).FullName) != null))
                        {
                            IPlugin plugin = (IPlugin)Activator.CreateInstance(type);
                            lPlugins.Add(new PluginPair(plugin));
                        }
                    }
                }
            }
            else { Directory.CreateDirectory(PathPlugins); }
        }

        public static void InitPlugins()
        {
            try
            {
                if (Utilities.RegistryApp.OpenSubKey("Disable") == null) { Utilities.RegistryApp.CreateSubKey("Disable"); }

                for (int i = 0; i < lPlugins.Count; i++)
                {
                    try
                    {
                        lPlugins[i].PluginMetaInfo = lPlugins[i].Plugin.Init();
                        if (lPlugins[i].PluginMetaInfo == null) { throw new Exception("PluginMetaInfo is null"); }

                        object dis;
                        if (Utilities.RegistryDisable.ExistKey(lPlugins[i].PluginMetaInfo.API, out dis))
                        {
                            lPlugins[i].SetDisableWithOutRegistry((bool) dis);
                        }
                        else
                        {
                            Utilities.RegistryDisable.SetValue(lPlugins[i].PluginMetaInfo.API, false);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(string.Format("{0} - Erreur lors de l’initialisation : {1}\nLocation : {2}", lPlugins[i].PluginMetaInfo.Name, ex.Message, ex.Source));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("InitPlugins : " + ex.Message);
            }
        }

        public static List<QueryResult> QueryPlugins(string query)
        {
            string FullQuery = query.Trim().ToLower();

            List<QueryResult> lQueryResult = new List<QueryResult>();
            try
            {
                if (FullQuery.Contains(" "))
                {
                    string Keyword = FullQuery.Substring(0, FullQuery.IndexOf(' '));
                    string QueryWithoutKeyword = FullQuery.Substring(FullQuery.IndexOf(' ') + 1);

                    foreach (PluginPair pp in lPlugins)
                    {
                        try
                        {
                            if (pp.Disable) { continue; }
                            if (pp.PluginMetaInfo.ActionKeywords != null)
                            {
                                if (pp.PluginMetaInfo.ActionKeywords.Contains(Keyword))
                                {
                                    List<QueryResult> qrp = pp.Plugin.Query(QueryWithoutKeyword);
                                    if (qrp != null) { lQueryResult.AddRange(qrp); }
                                }
                            }
                            else
                            {
                                List<QueryResult> qrp = pp.Plugin.Query(FullQuery);
                                if (qrp != null) { lQueryResult.AddRange(qrp); }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error " + pp.PluginMetaInfo.Name + " : " + ex.ToString());
                        }
                    }
                }
                else
                {
                    foreach (PluginPair pp in lPlugins)
                    {
                        try
                        {
                            if (pp.Disable) { continue; }
                            if (pp.PluginMetaInfo.ActionKeywords == null)
                            {
                                List<QueryResult> qrp = pp.Plugin.Query(FullQuery);
                                if (qrp != null) { lQueryResult.AddRange(qrp); }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error " + pp.PluginMetaInfo.Name + " : " + ex.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("QueryPlugins : " + ex.Message);
            }
            return lQueryResult;
        }

        public static List<PluginPair> GetPluginsPair()
        {
            return lPlugins;
        }
    }
}
