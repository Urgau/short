﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Short.Themes
{
    class ThemeManager
    {
        public static string[] ListThemes { get { return new string[] { "Default", "Green" }; } }
        public static string CurrentTheme { get; private set; }

        public static void SetTheme(string themename)
        {
            if (ListThemes.Contains(themename))
            {
                ResourceDictionary theme = new ResourceDictionary();
                theme.Source = new Uri(@"Themes\" + themename + ".xaml", UriKind.Relative);
                Application.Current.Resources.MergedDictionaries.Clear();
                Application.Current.Resources.MergedDictionaries.Add(theme);
                CurrentTheme = themename;
            }
            else
            {
                throw new Exception("Aucun thème n'a ce nom !");
            }
        }

        public static void LoadLastTheme()
        {
            //Write his code
            CurrentTheme = "Default";
        }
    }
}
