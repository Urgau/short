﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;

namespace Short
{
    sealed class HotKey
    {
        private const int VK_SHIFT = 0x10;
        private const int VK_CONTROL = 0x11;
        private const int VK_ALT = 0x12;
        private const int VK_WIN = 91;

        public event EventHandler KeysPressed;

        private Thread tKey;
        private Dispatcher disGUI;

        public HotKey()
        {
            disGUI = Dispatcher.CurrentDispatcher;
            tKey = new Thread(new ThreadStart(DetectKey));
            tKey.Start();
        }
        
        private void DetectKey()
        {
            int FirstKeyIndex = VK_ALT;
            int SecondKeyIndex = 83;
            while (true)
            {
                if (Utilities.KeyPress(FirstKeyIndex) && Utilities.KeyPress(SecondKeyIndex))
                {
                    while (Utilities.KeyPress(FirstKeyIndex) && Utilities.KeyPress(SecondKeyIndex)) { Thread.Sleep(50); }
                    disGUI.Invoke(new Action(() =>
                    {
                        KeysPressed(this, null);
                    }));
                }
                Thread.Sleep(50);
            }
        }
    }
}
