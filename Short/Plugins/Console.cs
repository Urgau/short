﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Short.Plugin;

namespace Short.Plugins
{
    sealed class Console : IPlugin
    {
        public PluginMetaInfo Init()
        {
            PluginMetaInfo pMetaInfo = new PluginMetaInfo("eef730f2ad069bf3b81ae7263df68a2d");
            pMetaInfo.Author = "Short";
            pMetaInfo.Name = "Console";
            pMetaInfo.Version = "Alpha 2";
            pMetaInfo.IconPath = @"Pictures\ConsolePlugin.png";
            pMetaInfo.Description = "Console est un plugin qui permet executer rapidement des commandes dans l'invité de commande window";
            pMetaInfo.Language = "Français";
            pMetaInfo.Website = null;
            pMetaInfo.ActionKeywords = new List<string>() { ">" };

            return pMetaInfo;
        }

        public List<QueryResult> Query(string query)
        {
            List<QueryResult> lQuerys = new List<QueryResult>();

            lQuerys.Add(new QueryResult(@"Pictures\Console.png", query, string.Empty, new Action(() =>
                {
                    Process.Start("cmd.exe", "/K " + query);
                })));

            return lQuerys;
        }
    }
}
