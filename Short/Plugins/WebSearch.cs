﻿using Short.Plugin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Short.Plugins
{
    sealed class WebSearch : IPlugin
    {
        public Dictionary<String, WebSiteMetaInfo> dWebSite = new Dictionary<String, WebSiteMetaInfo>();

        public PluginMetaInfo Init()
        {
            PluginMetaInfo pMetaInfo = new PluginMetaInfo("0bb2b3b9850e2141acb496aaf48a9f10");
            pMetaInfo.Author = "Short";
            pMetaInfo.Name = "WebSearch";
            pMetaInfo.Version = "Alpha 2";
            pMetaInfo.IconPath = @"Pictures\WebsearchPlugin.png";
            pMetaInfo.Description = "WebSearch est un plugin de base de Short qui permet de lancer facilement des recherches sur des sites internets.";
            pMetaInfo.Language = "Français";
            pMetaInfo.Website = null;

            dWebSite.Add("g", new WebSiteMetaInfo("https://www.google.fr/search?q={q}", @"Pictures\GoogleLogo.png"));
            dWebSite.Add("wiki", new WebSiteMetaInfo("http://fr.wikipedia.org/wiki/{q}", @"Pictures\WikipediaLogo.png"));
            dWebSite.Add("a", new WebSiteMetaInfo("https://www.amazon.fr/s?field-keywords={q}", @"Pictures\AmazonLogo.png"));
            dWebSite.Add("yt", new WebSiteMetaInfo("https://www.youtube.com/results?search_query={q}", @"Pictures\YoutubeLogo.png"));
            dWebSite.Add("q", new WebSiteMetaInfo("https://www.qwant.com/?q={q}", @"Pictures\QwantLogo.jpg"));
            dWebSite.Add("dico", new WebSiteMetaInfo("http://www.le-dictionnaire.com/definition.php?mot={q}", @"Pictures\DictionnaireLogo.png"));

            return pMetaInfo;
        }

        public List<QueryResult> Query(string query)
        {
            List<QueryResult> lQuerys = new List<QueryResult>();

            if (query.Contains(' '))
            {
                string cmd = query.Substring(0, query.IndexOf(' '));
                if (dWebSite.ContainsKey(cmd))
                {
                    WebSiteMetaInfo ws = dWebSite[cmd];

                    string q = query.Substring(cmd.Length + 1);
                    string path = ws.UrlSearch.Replace("{q}", q.Replace(" ", "%20"));

                    lQuerys.Add(new QueryResult(ws.IconPath, q, path));
                }
            }

            return lQuerys;
        }

        internal sealed class WebSiteMetaInfo
        {
            public string IconPath { get; private set; }
            public string UrlSearch { get; private set; }

            public WebSiteMetaInfo(string urlsearch, string iconPath)
            {
                UrlSearch = urlsearch; IconPath = iconPath;
            }
        }
    }
}
