﻿using Short.Plugin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Windows.Forms;

namespace Short.Plugins
{
    class CustomCommands : IPlugin
    {
        Dictionary<String, Tuple<String, String>> listCommands;

        #region IPlugin
        public PluginMetaInfo Init()
        {
            PluginMetaInfo pMetaInfo = new PluginMetaInfo("3a5ec3db5dfbafd429aa0eadf91d21d9");
            pMetaInfo.Author = "Short";
            pMetaInfo.Version = "Alpha 1";
            pMetaInfo.Name = "CustomCommands";
            pMetaInfo.Language = "English";
            pMetaInfo.IconPath = @"Pictures\CustomCommands.png";
            pMetaInfo.Description = "This is a plugin to create a few custom commands";

            LoadlistCommands();
            return pMetaInfo;
        }

        public List<QueryResult> Query(string query)
        {
            List<QueryResult> qrs = new List<QueryResult>();

            //System.Console.WriteLine("listCommands.ContainsKey(query) = " + listCommands.ContainsKey(query));
            if (listCommands.ContainsKey(query))
            {
                qrs.Add(getQueryResult(query));
            }
            else if (query.StartsWith("commands"))
            {
                if (query == "commands add")
                {
                    qrs.Add(getQueryResultAdd());
                }
                else if (query == "commands remove")
                {
                    foreach (string name in listCommands.Keys)
                    {
                        qrs.Add(getQueryResultRemove(name));
                    }
                }
                else if (query == "commands list")
                {
                    foreach (string name in listCommands.Keys)
                    {
                        qrs.Add(getQueryResult(name));
                    }
                }
            }

            return qrs;
        }
        #endregion

        #region QueryResult
        private QueryResult getQueryResult(string name)
        {
            Tuple<String, String> cmd = listCommands[name];
            return new QueryResult(cmd.Item1, name, null, new Action(() => { Process.Start(cmd.Item1, cmd.Item2); }));
        }

        private QueryResult getQueryResultRemove(string name)
        {
            Tuple<String, String> cmd = listCommands[name];
            return new QueryResult(cmd.Item1, name, "Enter to remove", new Action(() => { listCommands.Remove(name); SavelistCommands(); }));
        }

        private QueryResult getQueryResultAdd()
        {
            QueryResult qr = new QueryResult(null, "Commands : Add", null, new Action(() =>
            {
                string newCommand = Prompt.ShowDialog("Enter the new command : ", "CustomCommands").Trim().ToLower();

                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Title = "CustomCommands - Chose the file";
                ofd.ShowDialog();
                string newFile = ofd.FileName;
                string args = Prompt.ShowDialog("Enter args : ", "CustomCommands").Trim().ToLower();

                if (string.IsNullOrWhiteSpace(newCommand) == false && File.Exists(newFile))
                {
                    listCommands.Add(newCommand, new Tuple<string, string>(newFile, args));
                    SavelistCommands();
                }
            }));

            return qr;
        }
        #endregion

        #region listCommands
        private const string FileName = "CustomCommands.xml";

        private void LoadlistCommands()
        {
            Thread t = new Thread(new ThreadStart(() =>
            {
                if (File.Exists("CustomCommands.xml"))
                {
                    try
                    {
                        StreamReader sw = new StreamReader("CustomCommands.xml");
                        BinaryFormatter serializer = new BinaryFormatter();
                        listCommands = (Dictionary<string, Tuple<string, string>>)serializer.Deserialize(sw.BaseStream);
                        sw.Close();
                    }
                    catch (Exception ex)
                    {
                        System.Console.WriteLine(ex.ToString());
                        listCommands = new Dictionary<string, Tuple<string, string>>();
                    }
                }
                else
                {
                    listCommands = new Dictionary<string, Tuple<string, string>>();
                }
            }));
            t.Start();
        }

        private void SavelistCommands()
        {
            Thread t = new Thread(new ThreadStart(() =>
            {
                StreamWriter sw = new StreamWriter("CustomCommands.xml");
                BinaryFormatter serializer = new BinaryFormatter();
                serializer.Serialize(sw.BaseStream, listCommands);
                sw.Close();
            }));
            t.Start();
        }
        #endregion

        public class Prompt
        {
            public static string ShowDialog(string text, string caption)
            {
                Form prompt = new Form()
                {
                    Width = 480,
                    Height = 150,
                    FormBorderStyle = FormBorderStyle.FixedDialog,
                    Text = caption,
                    StartPosition = FormStartPosition.CenterScreen
                };
                Label textLabel = new Label() { Left = 5, Top = 10, Text = text };
                TextBox textBox = new TextBox() { Left = 5, Top = 50, Width = 450 };
                Button confirmation = new Button() { Text = "Ok", Left = 350, Width = 100, Top = 75, DialogResult = DialogResult.OK };
                confirmation.Click += (sender, e) => { prompt.Close(); };
                prompt.Controls.Add(textBox);
                prompt.Controls.Add(confirmation);
                prompt.Controls.Add(textLabel);
                prompt.AcceptButton = confirmation;

                return prompt.ShowDialog() == DialogResult.OK ? textBox.Text : "";
            }
        }
    }
}
