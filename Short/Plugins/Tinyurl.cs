﻿using Short.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace Short.Plugins
{
    class Tinyurl : IPlugin
    {
        public PluginMetaInfo Init()
        {
            PluginMetaInfo pMetaInfo = new PluginMetaInfo("219f77368929ec0602fdd4b83bb7522b");
            pMetaInfo.Author = "Short";
            pMetaInfo.Language = "French";
            pMetaInfo.Name = "Tinyurl";
            pMetaInfo.Version = "Version 1";
            pMetaInfo.ActionKeywords = new List<string>() { "tinyurl" };
            pMetaInfo.Description = "Tinyurl is a plugin for reduct url";
            pMetaInfo.IconPath = @"Pictures\TinyurlPlugin.png";

            return pMetaInfo;
        }

        Regex regex = new Regex(@"[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)");
        public List<QueryResult> Query(string url)
        {
            if (regex.IsMatch(url))
            {
                string shortenedUrl = string.Empty;
                if (ShortenUrl(url, out shortenedUrl))
                {
                    QueryResult qr = new QueryResult(@"Pictures\TinyurlPlugin.png", shortenedUrl, "Enter to copy", new Action(() => {
                        Clipboard.SetText(shortenedUrl);
                    }));
                    return new List<QueryResult>() { qr };
                }
            }

            return null;
        }

        private bool ShortenUrl(string url, out string shortenedUrl)
        {
            shortenedUrl = "";

            using (var client = new WebClient())
            {
                try
                {
                    shortenedUrl = client.DownloadString("http://tinyurl.com/api-create.php?url=" + url);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
    }
}
