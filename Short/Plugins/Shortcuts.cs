﻿using Short.Plugin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Interop;
using System.Windows.Media;

namespace Short.Plugins
{
    sealed class Shortcuts : IPlugin
    {
        public List<Shortcut> lFichier = new List<Shortcut>();

        public PluginMetaInfo Init()
        {
            PluginMetaInfo pMetaInfo = new PluginMetaInfo("98ff95f3afcf86d04f61c4b446463d21");
            pMetaInfo.Author = "Short";
            pMetaInfo.Name = "Shortcuts";
            pMetaInfo.Version = "Alpha 2";
            pMetaInfo.IconPath = @"Pictures\ShortcutsPlugin.png";
            pMetaInfo.Description = "Shortcuts est le plugin de base de Short, c'est lui qui vous permet de lancer vos applications.";
            pMetaInfo.Language = "Français";
            pMetaInfo.Website = null;

            List<string> ls = new List<string>();
            ls.AddRange(Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu) + @"\Programs", "*", SearchOption.AllDirectories));
            ls.AddRange(Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Microsoft\Windows\Start Menu\Programs", "*", SearchOption.AllDirectories));

            foreach (string path in ls)
            {
                lFichier.Add(new Shortcut(path, Path.GetFileNameWithoutExtension(path)));
            }

            return pMetaInfo;
        }

        public List<QueryResult> Query(string query)
        {
            List<QueryResult> lQuerysNonpertiant = new List<QueryResult>();
            List<QueryResult> lQuerysPertinant = new List<QueryResult>();

            ImageSourceConverter imgSrcConv = new ImageSourceConverter();
            foreach (Shortcut s in lFichier)
            {
                string NameLower = s.Name.ToLower();
                if (NameLower.Contains(query))
                {
                    QueryResult q = new QueryResult(s.Path, s.Name, s.Path);
                    if (NameLower.StartsWith(query)) { lQuerysPertinant.Add(q); }
                    else { lQuerysNonpertiant.Add(q); }
                }
            }

            List<QueryResult> lQuerys = new List<QueryResult>();
            lQuerys.AddRange(lQuerysPertinant); lQuerys.AddRange(lQuerysNonpertiant);
            return lQuerys;
        }

        internal sealed class Shortcut
        {
            public string Path { get; private set; }
            public string Name { get; private set; }

            public Shortcut(string path, string name)
            {
                Path = path; Name = name;
            }
        }
    }
}
