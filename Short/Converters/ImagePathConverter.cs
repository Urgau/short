﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Short
{
    sealed class ImagePathConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || String.IsNullOrWhiteSpace(value.ToString()) || value == DependencyProperty.UnsetValue)
            {
                return null;
            }
            
            return PathToImageSource(value.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        private static readonly List<string> ImageExtension = new List<string>
        {
            ".png",
            ".jpg",
            ".jpeg",
            ".gif",
            ".bmp",
            ".tiff",
            ".ico"
        };

        private static readonly List<string> SelfExtesion = new List<string>
        {
            ".exe",
            ".lnk",
            ".url"
        };

        public ImageSource PathToImageSource(String path)
        {
            path = Path.GetFullPath(path);
            if (!File.Exists(path)) { return null; }
            ImageSource img;

            if (Utilities.CacheImage.PathExist(path))
            {
                return Utilities.CacheImage.GetImageSource(path);
            }
            else
            {
                string ext = Path.GetExtension(path).ToLower();
                if (ImageExtension.Contains(ext))
                {
                    img = new BitmapImage(new Uri(path));
                }
                else if (SelfExtesion.Contains(ext))
                {
                    Icon ico = Icon.ExtractAssociatedIcon(path);
                    img = Imaging.CreateBitmapSourceFromHIcon(ico.Handle, System.Windows.Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
                }
                else { return null; }

                Utilities.CacheImage.AddImageSourceInCache(path, img);
            }
            
            return img;
        }
    }
}
