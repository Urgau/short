# Short - Documentation

### Sommaire
- Présentation
- Fonctionnement
- Fonctionnalité
- Plugin
- Auteur
- Crédits

### **Présentation**
**Short** est un logiciel similaire à Wox ou Alfred. Il a pour but de chercher est d'exécuter rapidement des logiciels, des commandes cmd, des recherches web, ... . Il n'est disponible que sur Windows.

### **Fonctionnement**
**Short**  utilise le Framework .NET et le C# pour le back-end. Et Window Présentation Form *(WPF)* pour l'interface graphique.

Si vous souhaitez plus d'informations sur le fonctionnement de Short. [Contacter-moi]("mailto:pro.lb@numericable.fr") ou regardé le code. :-) 

### **Fonctionnalité**
Voici les fonctionnalités disponibles pour le moments :
- Rechercher et lancer un logiciel
- Lancer des commandes dans la **cmd** en rajoutant **>** avant la commande
- Faire des recherches sur des sites web comme :
    - Amazon avec le "**a**"
    - Google avec le "**g**"
    - Wikipédia avec "**wiki**"
    - Qwant avec le "**q**"
    - YouTube avec "**yt**" 
    
Il y a aussi la possibilité de créer des plugins, en y incluent la classe Plugin.

### **Plugin**
*En cour de rédaction*

### **Auteur**
Loïc Branstett, développeur C#, VB.net, WPF, Java et Vaadin.

### **Credits**

Copyright (c) 2015 Branstett Loïc

L'autorisation est accordée, gracieusement, à toute personne acquérant une copie de ce logiciel et des fichiers de documentation associés (le "Logiciel"), de commercialiser le Logiciel sans restriction, notamment les droits d'utiliser, de copier, de modifier, de fusionner, de publier, de distribuer, de sous-licencier et / ou de vendre des copies du Logiciel, ainsi que d'autoriser les personnes auxquelles le Logiciel est fournie à le faire, sous réserve des conditions suivantes :

La déclaration de copyright ci-dessus et la présente autorisation doivent être incluses dans toutes copies ou parties substantielles de la Bibliothèque.

LE LOGICIEL EST FOURNIE "TELLE QUELLE", SANS GARANTIE D'AUCUNE SORTE, EXPLICITE OU IMPLICITE, NOTAMMENT SANS GARANTIE DE QUALITÉ MARCHANDE, D’ADÉQUATION À UN USAGE PARTICULIER ET D'ABSENCE DE CONTREFAÇON. EN AUCUN CAS, LES AUTEURS OU TITULAIRES DU DROIT D'AUTEUR NE SERONT RESPONSABLES DE TOUT DOMMAGE, RÉCLAMATION OU AUTRE RESPONSABILITÉ, QUE CE SOIT DANS LE CADRE D'UN CONTRAT, D'UN DÉLIT OU AUTRE, EN PROVENANCE DE, CONSÉCUTIF À OU EN RELATION AVEC LE LOGICIEL OU SON UTILISATION, OU AVEC D'AUTRES ÉLÉMENTS DU LOGICIEL.